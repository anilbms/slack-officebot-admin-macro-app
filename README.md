# Forge Hello World

This project contains a Forge app written in Javascript that displays `Hello World!` in a Confluence macro.

See [developer.atlassian.com/platform/forge/](https://developer.atlassian.com/platform/forge) for documentation and tutorials explaining Forge.

## Requirements

See [Set up Forge](https://developer.atlassian.com/platform/forge/set-up-forge/) for instructions to get set up.

# Slack app admin actions Forge macro app

This project contains a Forge macro app written in Javascript that displays **Slack app admin actions** for admins/HR in a Confluence macro. **Slack API** is to schedule messages.

## Requirements

You need the following:

- [Slack workspace](https://slack.com/intl/en-de/help/articles/212675257-Join-a-Slack-workspace)
- [Slack app](https://api.slack.com/apps)
- [Slack app token](https://slack.com/intl/en-de/help/articles/215770388-Create-and-regenerate-API-tokens) with following permission
  ![Slack office bot permission](./slack-app-bot-token-permission.png)

## Airtable
and authentification information and update your Airtable specific information in `src/api.js`.

```javascript
const AIRTABLE_API_KEY = '<YOUR_AIRTABLE_API_KEY>';
const ADD_IDEA_TO_AIRTABLE_URL = '<YOUR_AIRTABLE_BASE_CREATE_RECORD_URL>';
const GET_PRODUCT_IDEAS_FROM_AIRTABLE_URL =
  '<YOUR_AIRTABLE_BASE_GET_RECORDS_URL>';
```

## Quick demo

[![Slack office bot admin Forge app macro in confluence](http://img.youtube.com/vi/gRVY-OJHQ-E/0.jpg)](https://youtu.be/gRVY-OJHQ-E)

## Quick start

- Install dependencies by running:

  ```
  npm install
  ```

- Modify your app by editing the `src/api.tsx` file and your Slack app token.

- Build and deploy your app by running:

  ```
  forge deploy
  ```

- Install your app in an Atlassian site by running:

  ```
  forge install
  ```

### Notes

- Deploy your app, with the `forge deploy` command, any time you make changes to the code.
- Install your app, with the `forge install` command, when you want to install your app on a new site. Once the app is installed on a site, the site picks up the new app changes you deploy without needing to run the install command again.

### Support

See [Get help](https://developer.atlassian.com/platform/forge/get-help/) for how to get help and provide feedback.
If you have any questions or feedback, contact [Anil Kumar Krishnashetty](mailto:anilbms75@gmail.com) or on Twitter [@anilbms75](https://twitter.com/anilbms75).
