import API from '@forge/api';

const asyncForEach = async (array, callback) => {
    for (let index = 0; index < array.length; index++) {
        await callback(array[index], index, array);
    }
};

const token = process.env.codegeist2020slackapptoken;
const baseUrl = 'https://slack.com/api/';

// Cache Users and ConversationIds
let Users = [];
let ConversationIds = [];

export const schedulMessageToChannel = async ({
        channel,
        text,
        post_at
    }) =>
    await API.fetch(
        `${baseUrl}/chat.scheduleMessage?token=${token}&channel=${channel}&text=${text}&post_at=${post_at}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
        }
    )
    .then(() => true)
    .catch((error) => {
        console.log('schedulMessageToChannel fetch ERROR --->', error);
    });

export const getGeneralChannelUsers = async () => {
    await API.fetch(`${baseUrl}/channels.list?token=${token}`)
        .then((response) => {
            return response.json();
        })
        .then(({
            channels
        }) => {
            const {
                members
            } = channels.filter(
                (channel) => channel.name === 'general'
            )[0];
            Users = members;
            return members;
        })
        .catch((error) => {
            console.log('generalChannelId fetch ERROR --->', error);
        });
    return Users;
};

export const getConversationIds = async (users) => {
    const conversationIds = [];
    // Use the `chat.postMessage` method to send a message from this app
    await asyncForEach(users, async (user) => {
        const {
            channel: {
                id
            },
        } = await API.fetch(
                `${baseUrl}/conversations.open?token=${token}&users=${user}`
            )
            .then((response) => response.json())
            .catch((error) => {
                console.log('getConversationIds fetch ERROR --->', error);
            });
        conversationIds.push(id);
    });
    ConversationIds = conversationIds;
    return conversationIds;
};

export const schedulMessageAllUsers = async ({
    text,
    post_at
}) => {
    Users = await getGeneralChannelUsers();
    ConversationIds = await getConversationIds(Users);

    await asyncForEach(ConversationIds, async (channel) => {
        await schedulMessageToChannel({
            channel,
            text,
            post_at,
        });
    });

    return true;
};

export const getScheduledMessages = async () => {
    const scheduledTimes = [];
    const {
        scheduled_messages
    } = await API.fetch(
            `${baseUrl}/chat.scheduledMessages.list?token=${token}`
        )
        .then((response) => response.json())
        .catch((error) => {
            console.log('getScheduledMessages fetch ERROR --->', error);
        });

    scheduled_messages.forEach(({
        post_at
    }) => {
        if (scheduledTimes.indexOf(post_at) === -1) {
            scheduledTimes.push(post_at);
        }
    });

    return scheduledTimes.map((post_at) => {
        const {
            text
        } = scheduled_messages.find(({
            post_at: currentPostAt
        }) => {
            return currentPostAt === post_at;
        });
        return {
            post_at,
            text,
        };
    });
};

export const sendInstantMessageToChannelAllUsers = async (text) => {
    // Use cached Users if available
    Users = Users.length ? Users : await getGeneralChannelUsers();
    // Use cached ConversationIds if available
    ConversationIds = ConversationIds.length ?
        ConversationIds :
        await getConversationIds(Users);

    await asyncForEach(ConversationIds, async (channel) => {
        await API.fetch(
                `${baseUrl}/chat.postMessage?token=${token}&channel=${channel}&text=${text}`
            )
            .then((response) => response.json())
            .catch((error) => {
                console.log('sendInstantMessageToChannelAllUsers ERROR --->', error);
            });
    });
    return true;
};