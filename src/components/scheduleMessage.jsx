import ForgeUI, {
  Fragment,
  Text,
  Button,
  useState,
  ModalDialog,
  Form,
  TextArea,
  DatePicker,
  Select,
  Option,
  Lozenge,
} from '@forge/ui';
import moment from 'moment';
import { schedulMessageAllUsers, getScheduledMessages } from '../api';

const ScheduleMessage = () => {
  const [
    scheduleMessageModalDialogOpen,
    setScheduleMessageModalDialogOpen,
  ] = useState(false);

  const [isScheduleMessageSuccess, setIsScheduleMessageSuccesss] = useState(
    false
  );
  const handleSubmit = async ({ text, date, hour, minutes }) => {
    const dateArray = date.split('-').map((item) => parseInt(item));
    const post_at = moment(
      new Date(
        dateArray[0],
        dateArray[1] - 1, // For example:  May it's 4 not 5,
        dateArray[2],
        parseInt(hour),
        parseInt(minutes),
        0
      )
    ).unix();
    const success = await schedulMessageAllUsers({
      text,
      post_at,
    });
    if (success) {
      setIsScheduleMessageSuccesss(true);
    }
    await getScheduledMessages();
  };

  return (
    <Fragment>
      <Button
        text="schedule a message all"
        onClick={() => setScheduleMessageModalDialogOpen(true)}
      />
      {scheduleMessageModalDialogOpen && (
        <ModalDialog
          header="Schedule a message"
          onClose={() => {
            setScheduleMessageModalDialogOpen(false);
            setIsScheduleMessageSuccesss(false);
          }}
        >
          {!isScheduleMessageSuccess && (
            <Form onSubmit={handleSubmit}>
              <TextArea label="Message" name="text" isRequired="true" />
              <DatePicker name="date" label="Schedule Date" isRequired="true" />
              <Select label="Hour (hh)" name="hour" isRequired="true">
                <Option defaultSelected label="00" value="00" />
                <Option label="00" value="00" />
                <Option label="01" value="02" />
                <Option label="02" value="02" />
                <Option label="03" value="03" />
                <Option label="04" value="04" />
                <Option label="05" value="05" />
                <Option label="06" value="06" />
                <Option label="07" value="07" />
                <Option label="08" value="08" />
                <Option label="09" value="09" />
                <Option label="10" value="10" />
                <Option label="11" value="11" />
                <Option label="12" value="12" />
                <Option label="13" value="13" />
                <Option label="14" value="14" />
                <Option label="15" value="15" />
                <Option label="16" value="16" />
                <Option label="17" value="17" />
                <Option label="18" value="18" />
                <Option label="19" value="19" />
                <Option label="20" value="20" />
                <Option label="21" value="21" />
                <Option label="22" value="22" />
                <Option label="23" value="23" />
              </Select>
              <Select label="Minutes (mm)" name="minutes" isRequired="true">
                <Option defaultSelected label="00" value="00" />
                <Option label="00" value="00" />
                <Option label="05" value="05" />
                <Option label="10" value="10" />
                <Option label="15" value="15" />
                <Option label="20" value="20" />
                <Option label="25" value="25" />
                <Option label="30" value="30" />
                <Option label="35" value="35" />
                <Option label="40" value="40" />
                <Option label="45" value="45" />
                <Option label="50" value="50" />
                <Option label="55" value="55" />
              </Select>
            </Form>
          )}
          {isScheduleMessageSuccess && (
            <Text>
              <Lozenge
                text="Message scheduled successfully!"
                appearance="success"
              />
            </Text>
          )}
        </ModalDialog>
      )}
    </Fragment>
  );
};

export default ScheduleMessage;
