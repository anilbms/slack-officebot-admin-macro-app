import ForgeUI, {
  Fragment,
  Text,
  Button,
  useState,
  ModalDialog,
  Form,
  TextArea,
  Lozenge,
} from '@forge/ui';
import { sendInstantMessageToChannelAllUsers } from '../api';

const InstantMessageToAll = () => {
  const [
    instantMessageModalDialogOpen,
    setInstantMessageModalDialogOpen,
  ] = useState(false);

  const [
    isInstantMessageSentSuccess,
    setIsInstantMessageSentSuccess,
  ] = useState(false);

  const handleSubmit = async ({ text }) => {
    const success = await sendInstantMessageToChannelAllUsers(text);
    if (success) {
      setIsInstantMessageSentSuccess(true);
    }
  };

  return (
    <Fragment>
      <Button
        text="Send instant message to all"
        onClick={async () => {
          setInstantMessageModalDialogOpen(true);
          setIsInstantMessageSentSuccess(false);
        }}
      />
      {instantMessageModalDialogOpen && (
        <ModalDialog
          header="Send instant Slack bot app message to all"
          onClose={() => setInstantMessageModalDialogOpen(false)}
        >
          {!isInstantMessageSentSuccess && (
            <Form onSubmit={handleSubmit}>
              <TextArea label="Message" name="text" isRequired="true" />
            </Form>
          )}
          {isInstantMessageSentSuccess && (
            <Text>
              <Lozenge text="Message sent successfully!" appearance="success" />
            </Text>
          )}
        </ModalDialog>
      )}
    </Fragment>
  );
};

export default InstantMessageToAll;
