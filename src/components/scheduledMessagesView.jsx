import ForgeUI, {
  Fragment,
  Text,
  Button,
  useState,
  ModalDialog,
  Table,
  Cell,
  Row,
  Head,
} from '@forge/ui';
import { getScheduledMessages } from '../api';

const ScheduledMessagesView = () => {
  const [scheduledMessages, _] = useState(
    async () => await getScheduledMessages()
  );

  const [isOpen, setOpen] = useState(false);
  return (
    <Fragment>
      <Button text="Show scheduled messages" onClick={() => setOpen(true)} />
      {isOpen && (
        <ModalDialog header="Scheduled messages" onClose={() => setOpen(false)}>
          <Table>
            <Head>
              <Cell>
                <Text content="**Time (UTC)**" />
              </Cell>
              <Cell>
                <Text content="**Message**" />
              </Cell>
            </Head>
            {scheduledMessages &&
              scheduledMessages.map(({ post_at, text }) => (
                <Row>
                  <Cell>
                    <Text content={`${new Date(post_at)}`} />
                  </Cell>
                  <Cell>
                    <Text content={text} />
                  </Cell>
                </Row>
              ))}
          </Table>
        </ModalDialog>
      )}
    </Fragment>
  );
};

export default ScheduledMessagesView;
