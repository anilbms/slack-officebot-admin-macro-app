import ForgeUI, { render, Fragment, Macro } from '@forge/ui';
import InstantMessageToAll from './components/instantMessageToAll';
import ScheduleMessage from './components/scheduleMessage';
import ScheduledMessagesView from './components/scheduledMessagesView';

const App = () => {
  return (
    <Fragment>
      <InstantMessageToAll />
      <ScheduleMessage />
      <ScheduledMessagesView />
    </Fragment>
  );
};

export const run = render(<Macro app={<App />} />);
